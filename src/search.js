module.exports = (data, query) => {
  if (!query) return data

  const queries = Object.entries(query)
  if (!queries.length) return data

  let newData = data

  queries.forEach((query) => {
    const [queryKey, queryValue] = query
    const splittedKey = queryKey.split('.')

    data.forEach((personObject) => {
      const objectValue = splittedKey.reduce((object, property) => {
        return object.hasOwnProperty(property) ? object[property] : object
      }, personObject)

      if (queryValue !== objectValue) {
        newData = newData.filter((newPersonObject) => {
          return newPersonObject.id !== personObject.id
        })
      }
    })
  })

  return newData
}
